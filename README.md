# Sorting Algorithms

## Task 1

### Solve the task:

Implement direct selection sorting, bubble sorting and insertion sorting for an array of characters:

`(char[] array = new char[];)`
***

## Task 2

### Solve the tasks:

Implement direct selection sorting, bubble sorting and insertion sorting for an array of strings:

`(String[] array = new String[];)`

Java does not use the operators `>`, `<` to compare objects. 
To find out which of the strings is lexicographically larger or smaller than the other, 
you need to call the `compareTo()` method.

#### For example:

```
String str1 = "Ivanov";
String str2 = "Petrov";
String str3 = "Ivanov";
System.out.println(str1.compareTo(str2));
```

// the expression `str1.compareTo(str2)` will return an integer value less than zero,
because string str1 (Ivanov) is lexicographically smaller than string str2 (Petrov)

`System.out.println(str2.compareTo(str1));`

// the expression `str2.compareTo(str1)` will return an integer value greater than zero,
because string str2 (Petrov) is lexicographically larger than string str1 (Ivanov)

`System.out.println(str1.compareTo(str3));`

// the expression `str1.compareTo(str3)` will return an integer value equal to zero,
because string str1 (Ivanov) is lexicographically equal to string str3 (Ivanov)
***

## Task 3

### Solve the tasks:

Two arrays of strings are given. The names of students are stored in one array, 
and surnames are stored in the second (in cells with matching indexes). 
Sort the names and related surnames in alphabetical order (first sort by first names,
and if the students have the same names, then by last names). You should not create 
auxiliary arrays or combine strings with first and last names into one string.

#### For example:

The source array:

| Egor     | Artiom   | Egor      | Vlad  | Igor    | Ivan  | Kirill   |
|----------|----------|-----------|-------|---------|-------|----------|
| Anufriev | Abikenov | Kalinchuk | Ilyin | Loginov | Minin | Fomichev |

Array after sorting:

| Artiom   | Egor     | Egor      | Igor    | Ivan  | Kirill   | Vlad  |
|----------|----------|-----------|---------|-------|----------|-------|
| Abikenov | Anufriev | Kalinchuk | Loginov | Minin | Fomichev | Ilyin |
