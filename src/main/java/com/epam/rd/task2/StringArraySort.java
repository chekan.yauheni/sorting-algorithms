package com.epam.rd.task2;

import java.util.Arrays;

public class StringArraySort {
    public static void main(String[] args) {
        String[] surnames = new String[] {
                "Webb",
                "Berry",
                "Clarke",
                "Mcclain",
                "Diaz",
                "Hawkins",
                "Logan",
                "Johnson",
                "Luna",
                "Marshall",
                "Jones",
                "Jimenez",
                "Whitaker",
                "Strong",
                "Fleming",
                "Strickland",
                "Thomas",
                "Washington",
                "Martinez",
                "Orozco"
        };

        String[] bubbleSortArray = Arrays.copyOf(surnames, surnames.length);
        String[] selectSortArray = Arrays.copyOf(surnames, surnames.length);
        String[] insertSortArray = Arrays.copyOf(surnames, surnames.length);

        bubbleSort(bubbleSortArray);
        selectSort(selectSortArray);
        insertSort(insertSortArray);

        printArray("All names:", surnames);
        System.out.println();
        printArray("Bubble sorted array:", bubbleSortArray);
        printArray("Select sorted array:", selectSortArray);
        printArray("Insert sorted array:", insertSortArray);
    }

    public static void bubbleSort(String[] arrayToSort) {
        boolean arraySorted = false;
        while(!arraySorted) {
            arraySorted = true;
            for(int i = 1; i < arrayToSort.length; i++) {
                if(arrayToSort[i].compareTo(arrayToSort[i - 1]) < 0) {
                    String temp = arrayToSort[i];
                    arrayToSort[i] = arrayToSort[i - 1];
                    arrayToSort[i - 1] = temp;
                    arraySorted = false;
                }
            }
        }
    }

    public static void selectSort(String[] arrayToSort) {
        for(int i = 0; i < arrayToSort.length; i++) {
            int minPos = i;
            for(int j = i; j < arrayToSort.length; j++) {
                if(arrayToSort[j].compareTo(arrayToSort[minPos]) < 0) {
                    minPos = j;
                }
            }
            String temp = arrayToSort[i];
            arrayToSort[i] = arrayToSort[minPos];
            arrayToSort[minPos] = temp;
        }
    }

    public static void insertSort(String[] arrayToSort) {
        for(int i = 1; i < arrayToSort.length; i++) {
            String currentStr = arrayToSort[i];
            int previousIndex = i - 1;
            while(previousIndex >= 0 && arrayToSort[previousIndex].compareTo(currentStr) > 0) {
                arrayToSort[previousIndex + 1] = arrayToSort[previousIndex];
                arrayToSort[previousIndex] = currentStr;
                previousIndex--;
            }
        }
    }

    public static void printArray(String extendMsg, String[] arrayToPrint) {
        System.out.printf("%s %s\n", extendMsg, Arrays.toString(arrayToPrint));
    }
}
