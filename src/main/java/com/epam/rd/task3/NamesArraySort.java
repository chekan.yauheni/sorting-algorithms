package com.epam.rd.task3;

import com.epam.rd.task2.StringArraySort;

import java.util.Arrays;

public class NamesArraySort {
    public static void main(String[] args) {
        String[] names = new String[]{
                "Jonathan", "Kristy", "Sherri", "Michelle", "Tina", "Katherine", "Joel", "Jonathan", "Rebecca", "Darryl"
        };

        String[] surnames = new String[]{
                "Stone", "Collins", "Garcia", "Ryan", "Burns", "Hernandez", "Faulkner", "Shaffer", "Mullins", "Richardson"
        };

        String[] expectedArray = new String[]{
                "Darryl Richardson",
                "Joel Faulkner",
                "Jonathan Shaffer",
                "Jonathan Stone",
                "Katherine Hernandez",
                "Kristy Collins",
                "Michelle Ryan",
                "Rebecca Mullins",
                "Sherri Garcia",
                "Tina Burns"
        };
        StringArraySort.printArray("Expected array:     ", expectedArray);
        System.out.println();

        String[] copyBubbleNames = Arrays.copyOf(names, names.length);
        String[] copyBubbleSurnames = Arrays.copyOf(surnames, surnames.length);

        String[] copySelectNames = Arrays.copyOf(names, names.length);
        String[] copySelectSurnames = Arrays.copyOf(surnames, surnames.length);

        String[] copyInsertNames = Arrays.copyOf(names, names.length);
        String[] copyInsertSurnames = Arrays.copyOf(surnames, surnames.length);

        bubbleSort(copyBubbleNames, copyBubbleSurnames);
        selectSort(copySelectNames, copySelectSurnames);
        insertSort(copyInsertNames, copyInsertSurnames);

        printCombinedArrays("Bubble sorted array:", copyBubbleNames, copyBubbleSurnames);
        printCombinedArrays("Select sorted array:", copySelectNames, copySelectSurnames);
        printCombinedArrays("Insert sorted array:", copyInsertNames, copyInsertSurnames);
    }

    public static void bubbleSort(String[] namesArray, String[] surnamesArray) {
        boolean arraySorted = false;
        while (!arraySorted) {
            arraySorted = true;
            for (int i = 1; i < namesArray.length; i++) {
                if (namesArray[i].compareTo(namesArray[i - 1]) < 0) {
                    swapStrings(i, namesArray);
                    swapStrings(i, surnamesArray);

                    arraySorted = false;
                } else if (namesArray[i].compareTo(namesArray[i - 1]) == 0) {
                    if (surnamesArray[i].compareTo(surnamesArray[i - 1]) < 0) {
                        swapStrings(i, surnamesArray);

                        arraySorted = false;
                    }
                }
            }
        }
    }

    public static void selectSort(String[] namesArray, String[] surnamesArray) {
        for (int i = 0; i < namesArray.length; i++) {
            int minPos = i;
            for (int j = i; j < namesArray.length; j++) {
                if (namesArray[j].compareTo(namesArray[minPos]) < 0) {
                    minPos = j;
                } else if (namesArray[j].compareTo(namesArray[minPos]) == 0) {
                    if (surnamesArray[j].compareTo(surnamesArray[minPos]) < 0) {
                        minPos = j;
                    }
                }
            }
            selectSwapStrings(i, minPos, namesArray);
            selectSwapStrings(i, minPos, surnamesArray);
        }
    }

    public static void insertSort(String[] namesArray, String[] surnamesArray) {
        for (int i = 1; i < namesArray.length; i++) {
            String currentName = namesArray[i];
            String currentSurname = surnamesArray[i];
            int previousIndex = i - 1;
            while (previousIndex >= 0 && namesArray[previousIndex].compareTo(currentName) >= 0) {
                namesArray[previousIndex + 1] = namesArray[previousIndex];
                surnamesArray[previousIndex + 1] = surnamesArray[previousIndex];

                namesArray[previousIndex] = currentName;
                surnamesArray[previousIndex] = currentSurname;

                previousIndex--;
            }
        }
    }

    private static void swapStrings(int index, String[] arraySwap) {
        String temp = arraySwap[index];
        arraySwap[index] = arraySwap[index - 1];
        arraySwap[index - 1] = temp;
    }

    private static void selectSwapStrings(int currIndex, int minIndex, String[] arraySwap) {
        String temp = arraySwap[currIndex];
        arraySwap[currIndex] = arraySwap[minIndex];
        arraySwap[minIndex] = temp;
    }

    private static void printCombinedArrays(String extendMsg, String[] namesArray, String[] surnamesArray) {
        System.out.printf("%s [", extendMsg);
        for (int i = 0; i < namesArray.length; i++) {
            String msgFormat = "%s %s, ";
            if (i == (namesArray.length - 1)) {
                msgFormat = "%s %s";
            }
            System.out.printf(msgFormat, namesArray[i], surnamesArray[i]);
        }
        System.out.println("]");
    }
}
