package com.epam.rd.tasks1;

import java.util.Arrays;
import java.util.Random;

public class CharArraySort {
    public static void main(String[] args) {
        char[] generatedArray = generateCharArray(20);

        char[] bubbleSortArray = Arrays.copyOf(generatedArray, generatedArray.length);
        char[] selectSortArray = Arrays.copyOf(generatedArray, generatedArray.length);
        char[] insertSortArray = Arrays.copyOf(generatedArray, generatedArray.length);

        printArray("Init array to sort:", generatedArray);
        System.out.println();

        bubbleSort(bubbleSortArray);
        selectSort(selectSortArray);
        insertSort(insertSortArray);

        printArray("Bubble sorted array:   ", bubbleSortArray);
        printArray("Selection sorted array:", selectSortArray);
        printArray("Insertion sorted array:", insertSortArray);
    }


    public static char[] generateCharArray(int arrayLength) {
        Random rnd = new Random();
        char[] newArray = new char[arrayLength];
        for(int i = 0; i < newArray.length; i++) {
            char randomChar = (char) ('a' + rnd.nextInt(26)); // 26 stands for number of latin letters in ascii
            newArray[i] = randomChar;
        } return newArray;
    }

    public static void bubbleSort(char[] arrayToSort) {
        boolean arraySorted = false;
        while(!arraySorted) {
            arraySorted = true;
            for(int i = 1; i < arrayToSort.length; i++) {
                if(arrayToSort[i] < arrayToSort[i - 1]) {
                    char temp = arrayToSort[i];
                    arrayToSort[i] = arrayToSort[i - 1];
                    arrayToSort[i - 1] = temp;
                    arraySorted = false;
                }
            }
        }
    }

    public static void selectSort(char[] arrayToSort) {
        for(int i = 0; i < arrayToSort.length; i++) {
            int minPos = i;
            for(int j = i; j < arrayToSort.length; j++) {
                if(arrayToSort[j] < arrayToSort[minPos]) {
                    minPos = j;
                }
            }
            char temp = arrayToSort[i];
            arrayToSort[i] = arrayToSort[minPos];
            arrayToSort[minPos] = temp;
        }
    }

    public static void insertSort(char[] arrayToSort) {
        for(int i = 1; i < arrayToSort.length; i++) {
            char currentChar = arrayToSort[i];
            int previousIndex = i - 1;
            while(previousIndex >= 0 && arrayToSort[previousIndex] > currentChar) {
                arrayToSort[previousIndex + 1] = arrayToSort[previousIndex];
                arrayToSort[previousIndex] = currentChar;
                previousIndex--;
            }
        }
    }

    public static void printArray(String extentMsg, char[] arrayToPrint) {
        System.out.printf("%s %s\n", extentMsg, Arrays.toString(arrayToPrint));
    }
}
